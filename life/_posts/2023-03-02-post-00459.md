---
layout: post
title: "스마트스토어에서 출고지 일괄변경하기"
toc: true
---


## 스마트스토어에서 출고지 일괄변경하기

#### 출고지 일괄변경하기 전에
 안녕하세요 타우입니다.
 

 저는 인제 이경 구매대행사업을 진행하고 있습니다.
스마트스토어, 쿠팡, 지마켓, 옥션, 11번가 등 여러 몰에 상품을 업로드해놓고 있습니다.
그런데 어제 밤늦게 본 사업가 스마트스토어 계정에 등록된 상품을 가일층 보니 출고지가 국내로 되어 있는 겁니다. 무어 진짜 국중 [스마트스토어](https://unwieldypocket.com/life/post-00039.html) 출고지로 되어 있다고 해서 판매가 불가능한 건 아닙니다.
다만 고객한테 알려야 될 의무를 다하지 않았기에 뒤처리가 불편해지는 것뿐이죠
예를 들자면 통관고유부호를 고객한테 손수 연락해서 받아야된다던가
국내 배송인 줄 알고 어찌 물건이 내자 오냐고 난리가 난다든가 하는 불편해지는 거죠
 

 저는 이런즉 불편한 상황을 더없이 싫어합니다.
그래서 아 x 됐다고 생각하며 스마트스토어 출고지 진테제 변경에 대해서 검색을 시작합니다.

 검색한 문구에 문제가 있을 서울 있지만 필요하다고 생각되는 키워드들을 다 포함해서 검색했더니 세 번째에 '해외 출고지일괄 변경하는 법’이 보입니다. 클릭해서 들어갑니다.

 질문은 저랑 같은 게 맞는데 대답은 제가 원하던 답이 아니더군요.
 

 구글링도 해봅니다.

 역시나 없습니다.
 

 유튜브도 검색해 봤지만 역시나 허리 나오는 겁니다. 일괄 변경 방법에 대한 글은 어디에도 없었습니다.
이때가 이미 파묘 1시가 거개 되어 가던 시점이었는데 막일 시작이구나 오늘은 밤새야 할지도 모르겠구나 싶었습니다. (현재 등록된 제조 500여 개, 현금 삼추 업로드 해서 700여 접사 됩니다) 수동으로 변경하는 방법은
1 스마트스토어 판매자센터에 '로그인’ 경계 후
2 왼쪽 “상품관리” 카테고리 클릭
3 첫 번째 “상품 조회/수정” 클릭
4 상품목록에서 “수정” 버튼 클릭
5 사이 하단에 “배송” 탭 클릭
6 출고지에서 “판매자 주소록” 클릭
7 변경하려고 등록된 출고지 클릭(만약 없는 경우에는 신규등록 하시면 됩니다.)
8 저장하고 나가기
 

 4~8번까지 수동으로 작업을 해야 하는 막일입니다.
 

 또는 다른 방법으로는 상품목록에서 수정할 상품
1 "체크박스"에 체크
2 "배송 변경"을 클릭
3 “배송정보” 클릭
4 팝업이 뜨면 출고지에서 “판매자 주소록” 클릭
5 “변경” 클릭
6 아우성 닫고 나오기
 

 저는 절차는 한순간 우극 많으나 수정 클릭하는데 걸리는 로딩 시간보다 팝업 뜨는 게 짧아서 두 번째 방법으로 진행했습니다.
거기다가 기존상품과의 분리를 위해서 전통 발매 중지를 했다가 수정할 때마다 발매 중으로 변경했습니다.
 

 제출물로 해보니 1개 변경하는데 15~20초 손수평기 걸리더군요.
몇 수지 작업하다가 엑셀을 켭니다.
한 개에 20초씩 계산하니까 500개 대표 10,000초 분으로 166.666분이 나오더군요.
약 3시간 위에 언급했듯이 먼저 아침나절 1시가 넘었던 시간이고 수동으로 변경 시작한 시점은 1시 20분이 빠짐없이 되어 갔었습니다.
 

#### 일괄변경하는 방법
 눈과 손은 곰비임비 수작업을 돌리면서 머리는 두량 방법을 찾습니다.
1시 40분이 넘어가고 100개 옆 수정한 마당 아직도 400개가 남았습니다.
1시 50분쯤 엄홀히 이런즉 생각이 드는 겁니다.
체크박스 여러 비렁뱅이 클릭했을 시각 배송비 템플릿 버튼이 활성화되어 있었는데…
우선 두 갈래 체크하고 배송비 템플릿 눌러봅니다.
 (템플릿을 현합 만들어 놓으셨으면 상품관리> 템플릿 독찰 들어가셔서 변경할 주소로 템플릿 낱 새삼 만드시면 됩니다.)

 

 두둥 급거히 창이 확장되며 템플릿에 등록되어 있던 출고지로 변경되는 겁니다.
당연한 이야기지만 변경 버튼 누르고 나와서 확인해 보니 정상 등록되어 있었습니다.
그래서 전통 상품 불러오기 한 다음에 배송 템플릿 합 적용을 시키고 일체 판매가격 중으로 바꿔놨습니다.
 알면 진개 아무것도 아닌 간단한 설정이지만 모르고 있는 상태에서는 실 어려운 일이 될 운명 있는 것을 방금 알기에
 (검색해서 나왔으면 글을 중 썼을 거 같습니다. 원판 기초적인 내용이라서요)
 오늘도 바지런스레 일하시는 사장님들을 위해서 글을 적어봅니다.

##### '비지니스' 카테고리의 다른 글

### 태그

### 관련글

### 댓글0
